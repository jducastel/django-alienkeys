from django.contrib import admin
from .models import AlienDB, AlienKey , AlienDBContentType

# @admin.register(AlienDBContentType)
class AlienDBContentTypeInline(admin.TabularInline):
    model = AlienDBContentType
    extra = 1

@admin.register(AlienDB)
class DBAdmin(admin.ModelAdmin):
    list_display = ('ref', 'name')
    inlines = [AlienDBContentTypeInline]

@admin.register(AlienKey)
class KeyAdmin(admin.ModelAdmin):
    list_filter = ('alien_db', 'content_type__app_label', 'content_type')
    list_display = ('alien_db', 'content_type', 'content_object', 'value')
    list_display_links = ('content_object', 'value',)
