from django.apps import AppConfig


class AlienkeysConfig(AppConfig):
    name = 'alienkeys'
