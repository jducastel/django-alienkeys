"""
TO DO, Maybe ?
- (manually) define relevant content types for an alien db
- type conversion / serialization on keys
"""

import logging

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

logger = logging.getLogger(__name__)


class ContentTypeProxy(ContentType):
    """ This allows us to have more readable content types in the admin """
    class Meta:
        proxy = True

    def __str__(self):
        return "{} : {}".format(self.app_label, self.name)


class AlienDB(models.Model):
    """ External database for which we want to stores keys to our models and instances """
    ref = models.SlugField('Reference', unique=True, max_length=25, help_text='Reference to use in project source code')
    name = models.CharField('External database / app name', max_length=250)
    description = models.CharField('Description', max_length=2000, blank=True, null=True)
    url = models.URLField('External app / DB URL', max_length=500, blank=True, null=True)
    # key_type_choices = [('numeric', 'Numeric'), ('uuid', 'UUID')]
    # key_type = models.CharField(max_length=50, choices=key_type_choices, default='numeric')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Alien database'
        verbose_name_plural = 'Alien databases'

    def set_key(self, content_object, key_value):
        """ set key value for an object."""
        try:
            key = self.get_key(content_object)
            key.value = key_value
            key.save()
            logger.debug("saved new value {} as key in {} AlienDB for {}".format(key_value, self, content_object))
        except AlienKey.DoesNotExist: # creating one
            key = AlienKey(alien_db=self, content_object=content_object, value=key_value)
            key.save()
            logger.debug("saved new key {} in {} AlienDB for {}".format(key_value, self, content_object))

    def get_key(self, content_object):
        """ get key for a content object (model instance) within this alien database """
        # cannot use directly self.keys.get(content_object)
        # so we have to filter on on both fields that defines the pk
        content_type = ContentType.objects.get_for_model(content_object)
        return self.keys.get(content_type=content_type, object_id=content_object.pk)

    def get_object_missing_keys(self):
        """ for each content type listed as relevant for this db,
        return all objects

        """


class AlienDBContentType(models.Model):
    """ Content Types for which we want to store keys for an external database """
    # relationship fields
    alien_db = models.ForeignKey(AlienDB, verbose_name='Alien database', on_delete=models.CASCADE, related_name='content_types')
    content_type = models.ForeignKey(ContentTypeProxy, on_delete=models.CASCADE)
    description = models.CharField("Description", blank=True, max_length=2000, help_text="May store details about the relationship")

    def get_missing_keys_qs(self):
        """ for each object of this alien db + content type,
        returns a queryset of the objects which don't have a key """
        # first, list object ids for which we have a key
        keys_qs = AlienKey.objects.filter(alien_db=self.alien_db, content_type=self.content_type)
        having_key_ids = [k.object_id for k in keys_qs]
        # getting the model class for this content type
        model_class = self.content_type.model_class()
        # then building the QS
        return model_class.objects.exclude(pk__in=having_key_ids)


class AlienKey(models.Model):
    """ Alien key in an external database for a local model instance"""
    # generic relation to local object through contenttypes
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    # not PositiveIntegerField to support non-numeric PK
    object_id = models.CharField(max_length=250)
    # local model instance
    content_object = GenericForeignKey('content_type', 'object_id')
    # relation to alien database
    alien_db = models.ForeignKey(AlienDB, verbose_name='Alien database', on_delete=models.CASCADE, related_name='keys')
    value = models.CharField(max_length=500, verbose_name='Value of the key for this instance on the alien database')

    def __str__(self):
        return "{} on {}".format(self.content_object, self.alien_db)

    class Meta:
        verbose_name = 'Alien key'
        verbose_name_plural = 'Alien keys'
        constraints = [
            models.UniqueConstraint(
                fields=['alien_db', 'content_type', 'object_id'],
                name='unique_on_alien_db')]
