"""


"""

from datetime import datetime
import logging

from django.db import models
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from django.conf import settings

from ypareo.rest_client import YpareoRestClient
from alienkeys.models import AlienDB, AlienKey, AlienDBContentType

# from formations.models import YearCatalog, Formation, VersionFormation
# from formations.utils.ypareo_ws import YpareoWebServices

logger = logging.getLogger(__name__)
# https://reinout.vanrees.org/weblog/2017/03/08/logging-verbosity-managment-commands.html
if settings.DEBUG:
    logger.setLevel(logging.DEBUG)


class Command(BaseCommand):
    # args = 'from_catalog to_catalog'
    help = """check_alien_keys <alien_db>
    check existence of alien keys for an alien DB

    for each content types registered for given AlienDB,
    checks the existence of an AlienKey for each model instance of the content type
    """

    def handle(self, db_ref, do_list, **options):
        logger.debug(f"checking keys for {db_ref}")
        try:
            alien_db = AlienDB.objects.get(ref=db_ref)
            for ct in alien_db.content_types.all():
                logger.debug(f"searching missing keys for {ct.content_type}")
                qs = ct.get_missing_keys_qs()
                if do_list:
                    logger.info(f"{ct.content_type} {qs.count()} objects missing key in AlienDB {db_ref}")
                    for item in qs:
                        logger.info(f"{item} has no key in AlienDB {db_ref}")
                else:
                    logger.info(f"found {qs.count()} missing keys in AlienDB {db_ref} : {qs}")
        except AlienDB.DoesNotExist:
            logger.error(f"could not find AlienDB {db_ref}")


    def add_arguments(self, parser):
        pass
        # cf https://docs.python.org/3/library/argparse.html#module-argparse
        # Positional arguments
        # parser.add_argument('poll_id', nargs='+', type=int)
        parser.add_argument('db_ref')
        # parser.add_argument('content_type', nargs='?', default=None)
        # optional arguments
        parser.add_argument('-l', '--list', action='store_true', dest="do_list", help="list every missing key")
